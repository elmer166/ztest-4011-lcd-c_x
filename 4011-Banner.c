/*! \file 4011-Banner.c
 *
 *  \brief Display the application's startup banner
 *
 *  The compilation date and time are displayed so that we know we
 *  aren't running a stale version.  Not entirely correct since the
 *  date won' be updated unless this file is recompiled, which
 *  basically implies a full clean and build.
 */
#include "lcd.h"
#include "delay.h"

void Banner( void )
{
  LCDclear ();
  Delay( Delay_1S_Cnt );

  // Display the compile date and time so we know we have the right compile
  LCDputs(__DATE__);
  LCDline2();
  LCDputs(__TIME__);
  Delay( Delay_1S_Cnt );
}
