/*! \file 4011-DemoLCDleft.c
 *
 *  \brief Demonstrate LCDleft() function
 *
 *  Draw a line of underbars on the top line, and a row
 *  of dashes on the second.  Then write the digits
 *  nine through zero starting fro the right, with a
 *  space between using LCDleft() so as not to disturb
 *  the previously written underbars between the digits.
 *  Then do the second thing on the second line using
 *  letters instead of digits.
 */

#include "lcd.h"
#include "delay.h"

void DemoLCDleft( void )
{
  int i;

  // Now we will write the digits 0 through 9 with a space
  // between each, from right to left, demonstrating the
  // LDCleft function
  LCDclear ();
  LCDputs ("Demonstrate ");
  LCDline2 ();
  LCDputs ("LCDleft() func.");
  Delay (Delay_1S_Cnt);

  LCDclear ();
  LCDputs ("____________________");
  LCDline2 ();
  LCDputs ("--------------------");
  LCDposition (20);
  for (i = 0; i < 10; i++)
    {
      LCDleft ();
      LCDleft ();
      LCDletter (0x39 - i);
      LCDleft ();
      Delay (Delay_1S_Cnt / 10);
    }
  LCDline2 ();
  LCDposition (0x40 + 18);
  for (i = 0; i < 8; i++)
    {
      LCDleft ();
      LCDleft ();
      LCDletter ('h' - i);
      LCDleft ();
      Delay (Delay_1S_Cnt / 10);
    }
  Delay (Delay_1S_Cnt / 2);
}
