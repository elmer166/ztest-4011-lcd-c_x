/*! \file 4011-DemoLCDposition.c
 *
 *  \brief Demonstrate LCDposition()
 *
 *  First, fill the display with trash, then display a message,
 *  top line right to left, bottom line left to right.  Then
 *  display a second message the same way.
 *
 *  Then display four messages in rapid succession, leaving
 *  barely enough time to see the messages, then display the
 *  same messages left to right, showing LCDposition as fast
 *  as possible.  The last character of each line is the message
 *  number giving some opportunity to detect any display
 *  errors in the rapid blast of messages.
 */
#include "lcd.h"
#include "delay.h"

// Four messages used for tests.  Note that the 16th character
// is 1, 2, 3 or 4 allowing recognition that the message was
// displayed properly even when erased quickly
//! Message 1 for display forward and backward
char szMessage1a[] = "am.  But why   1";
//! Message 1 for display forward and backward second line
char szMessage1b[] = "will you say   1";
//! Message 2 for display forward and backward
char szMessage2a[] = "that I am mad? 2";
//! Message 2 for display forward and backward second line
char szMessage2b[] = "The disease had2";
//! Message 3 for display forward and backward
char szMessage3a[] = "sharpened my   3";
//! Message 3 for display forward and backward second line
char szMessage3b[] = "senses, not    3";
//! Message 4 for display forward and backward
char szMessage4a[] = "destroyed, not 4";
//! Message 4 for display forward and backward second line
char szMessage4b[] = "dulled them    4";
//! Message 5 used for display from ends
char szMessage5a[] = "True, nervous,  ";
//! Message 5 used for display from ends second line
char szMessage5b[] = "very very dread-";
//! Message 6 used for display from ends
char szMessage6a[] = "fully nervous I ";
//! Message 6 used for display from ends second line
char szMessage6b[] = "had been and I  ";

void DemoLCDposition( void )
{
  int i;

  LCDclear ();
  LCDputs ("Demonstrate ");
  LCDline2 ();
  LCDputs ("LCDposition() ");
  Delay (Delay_1S_Cnt);

  LCDclear ();
  LCDputs ("ZZ\377ZZZZ\377ZZZZ\377ZZZ");
  LCDline2 ();
  LCDputs ("MMMM\377MM\377MMMM\377MM\377");

  for (i = 0; i < 16; i++)
    {
      Delay (Delay_15mS_Cnt);
      LCDposition (15 - i);
      LCDletter (szMessage5a[15 - i]);
      Delay (Delay_15mS_Cnt);
      LCDposition (0x40 + i);
      LCDletter (szMessage5b[i]);
    }
  Delay (Delay_1S_Cnt);
  for (i = 0; i < 16; i++)
    {
      Delay (Delay_15mS_Cnt);
      LCDposition (15 - i);
      LCDletter (szMessage6a[15 - i]);
      Delay (Delay_15mS_Cnt);
      LCDposition (0x40 + i);
      LCDletter (szMessage6b[i]);
    }
  Delay (Delay_1S_Cnt);
  LCDclear ();
  LCDputs ("Four messages in");
  LCDline2 ();
  LCDputs ("rapid succession.");
  Delay (Delay_1S_Cnt);

  // First, four messages in rapid succession
  LCDclear ();
  LCDputs (szMessage1a);
  LCDline2 ();
  LCDputs (szMessage1b);
  Delay (Delay_1S_Cnt / 6);

  LCDclear ();
  LCDputs (szMessage2a);
  LCDposition (0x40);
  LCDputs (szMessage2b);
  //while ( 1 ) ;
  Delay (Delay_1S_Cnt / 6);

  LCDclear ();
  LCDputs (szMessage3a);
  LCDposition (0x40);
  LCDputs (szMessage3b);
  Delay (Delay_1S_Cnt / 6);

  LCDclear ();
  LCDputs (szMessage4a);
  LCDposition (0x40);
  LCDputs (szMessage4b);
  Delay (Delay_1S_Cnt / 2);

  // Now the same four messages, this time testing cursor
  // positioning by positioning and writing each character,
  // starting from the last and proceeding to the first

  LCDclear ();
  LCDputs ("Same four writen");
  LCDline2 ();
  LCDputs ("right to left    ");
  Delay (Delay_1S_Cnt);

  for (i = 15; i >= 0; i--)
    {
      LCDposition (i);
      LCDletter (szMessage1a[i]);
      LCDposition (0x40 + i);
      LCDletter (szMessage1b[i]);
    }
  Delay (Delay_1S_Cnt / 6);

  for (i = 15; i >= 0; i--)
    {
      LCDposition (i);
      LCDletter (szMessage2a[i]);
      LCDposition (0x40 + i);
      LCDletter (szMessage2b[i]);
    }
  Delay (Delay_1S_Cnt / 6);

  for (i = 15; i >= 0; i--)
    {
      LCDposition (i);
      LCDletter (szMessage3a[i]);
      LCDposition (0x40 + i);
      LCDletter (szMessage3b[i]);
    }
  Delay (Delay_1S_Cnt / 6);

  for (i = 15; i >= 0; i--)
    {
      LCDposition (i);
      LCDletter (szMessage4a[i]);
      LCDposition (0x40 + i);
      LCDletter (szMessage4b[i]);
    }
  Delay (Delay_1S_Cnt / 2);
}
