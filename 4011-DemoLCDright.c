/*! \file 4011-DemoLCDright.c
 *
 *  \brief Demonstrate LCDright() function
 *
 *  Draw letters on the display separated by underbars,
 *  then slowly replace the letters with digits while not
 *  disturbing the underbars to again show LCDright.
 *  Similarly, the second line starts with digits which
 *  are replaced by letters.
 */

#include "lcd.h"
#include "delay.h"

void DemoLCDright( void )
{
  int i;

  LCDclear ();
  LCDputs ("Demonstrate ");
  LCDline2 ();
  LCDputs ("LCDright() func.");
  Delay (Delay_1S_Cnt);

  LCDclear ();
  LCDputs ("_a_b_c_d_e_f_g_h_i_j_");
  LCDline2 ();
  LCDputs ("_1_2_3_4_5_6_7_8_9_0_");

  LCDhome ();
  for (i = 0; i < 10; i++)
    {
      LCDright ();
      LCDletter (0x30 + i);
      Delay (Delay_1S_Cnt / 10);
    }
  LCDline2 ();
  for (i = 0; i < 10; i++)
    {
      LCDright ();
      LCDletter ('a' + i);
      Delay (Delay_1S_Cnt / 4);
    }
  Delay (Delay_1S_Cnt / 2);
}
