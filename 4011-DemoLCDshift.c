/*! \file 4011-DemoLCDshift.c
 *
 *  \brief Demonstrate LCDshift() function
 *
 *  Display a pattern of digits on the first line and
 *  letter on the second.  The shift the pattern off to
 *  the right of the display and back
 */

#include "lcd.h"
#include "delay.h"

void DemoLCDshift( void )
{
  int i;

  // Demonstrate LCDshift by shifting the display right
  // nineteen times
  LCDclear ();
  LCDputs ("Demonstrate ");
  LCDline2 ();
  LCDputs ("LCDshift() function");
  Delay (Delay_1S_Cnt);

  LCDclear ();
  LCDputs (" 1 2 3 4 5 6 7 8 ");
  LCDline2 ();
  LCDputs ("  a b c d e f g h ");

  for (i = 0; i < 19; i++)
    {
      LCDshift ();
      Delay (Delay_1S_Cnt / 10);
    }
  // and then shift back again
  for (i = 0; i < 19; i++)
    {
      LCDcommand (0x18);
      Delay (Delay_1S_Cnt / 10);
    }
  Delay (Delay_1S_Cnt / 2);
}
