/*! \file 4011-DemoLongScroll.c
 *
 *  \brief Scroll a long message
 *
 *  A long message is displayed - the first 16 characters
 *  normally, then the remainder added to the right of
 *  the display and the remaining letters scrolled to the
 *  left.
 */

#include <string.h>
#include "lcd.h"
#include "delay.h"

//! Message 7 used for display of a longer string
char szMessage7[] = "It is impossible to say how first the idea entered my brain; but once conceived, it haunted me day and night. Object there was none. Passion there was none. I loved the old man. He had never wronged me. He had never given me insult. For his gold I had no desire. I think it was his eye! yes, it was this! He had the eye of a vulture --a pale blue eye, with a film over it. Whenever it fell upon me, my blood ran cold; and so by degrees --very gradually --I made up my mind to take the life of the old man, and thus rid myself of the eye forever.";

void DemoLongScroll( void )
{
  int i;

  // Scroll a long message
  LCDclear ();
  LCDposition (0);
  for (i = 0; i < strlen (szMessage7); i++)
    {
      LCDposition (i % 40);
      LCDletter (szMessage7[i]);
      if (i > 15)
        LCDcommand (0x18);
      Delay (Delay_1S_Cnt / 10);
    }
  Delay (Delay_1S_Cnt / 2);
}
