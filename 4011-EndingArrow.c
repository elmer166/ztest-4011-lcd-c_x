/*! \file 4011-EndingArrow.c
 *
 *  \brief Mark the end of the loop with a scrolling arrow
 *
 *  Draw an arrow (==>) and scroll it off to the right
 *  of the display.
 */

#include "lcd.h"
#include "delay.h"

void EndingArrow( void )
{
  int i;

  // Finally draw an arrow on the clear display and cause it
  // to move across the display with LCDshift
  LCDclear ();
  LCDputs ("==>");
  for (i = 0; i < 20; i++)
    {
      LCDshift ();
      Delay (Delay_1S_Cnt / 6);
    }
  Delay (Delay_1S_Cnt / 2);
}
