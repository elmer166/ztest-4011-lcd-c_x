/*! \file 4011-Initialize.c
 *
 *  \brief Initialize peripherals for 4011-LCD-C
 *
 *  This function initializes the ports, timers, LCD and PWM.
 */
#include <p30Fxxxx.h>
#include "lcd.h"

//! Angle used to vary the intensity of the LCDs
float theta;

//! Initialize ports and perioherals
/*! Initialize() initializes the LED ports, Timers 2 and 3,
 *  PWM 2, 3 and 4, and the LCD.
 */
void Initialize (void)
{
  // Make PORTD outputs
  TRISD &= 0xfff1;
  LATD |= 0x000e;

  // Set up timer 2 for PWM
  TMR2 = 0;         // Clear timer 2
  PR2 = 1000;       // Timer 2 counter to 1000
  T2CON = 0x8010;   // Fosc/4, 1:4 prescale, start TMR2

  // Set up PWM on OC2 (RD1)
  OC2RS = 1024;     // PWM 2 duty cycle
  OC2R = 0;         //
  OC2CON = 0x6;     // Set OC2 to PWM mode, timer 2

  // Set up PWM on OC3 (RD2)
  OC3RS = 1024;     // PWM 3 duty cycle
  OC3R = 0;         //
  OC3CON = 0x6;     // Set OC4 to PWM mode, timer 2

  // Set up PWM on OC4 (RD3)
  OC4RS = 1024;     // PWM 4 duty cycle
  OC4R = 0;         //
  OC4CON = 0x6;     // Set OC4 to PWM mode, timer 2

  // Set up timer 3 for LED duty cycle
  theta = 0.1;      // Starting LED brightness
  TMR3 = 0;         // Clear timer 3
  PR3 = 4000;       // Timer 3 counter to 4000
  T3CON = 0x8030;   // Fosc/4, 1:256 prescale, start TMR3
  _T3IE = 1;        // Enable T3 interrupt

  LCDinit ();

}
