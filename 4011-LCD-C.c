/*! \file 4011-LCD-C.c
 *
 *  \brief LCD demo on dsPIC30F4011 "dsPIC-EL" board
 *
 *   This version for 16x2 LCDs such as the "blue" Newhaven,
 *   Craig's "Chinese" LCDs, etc.
 *
 *   4011-LCD.X is for the 2x20's such as the Seiko and
 *   the "white" Newhaven.
 *
 *
 * Author: jjmcd
 *
 * Created on September 2, 2012, 10:03 AM
 */

/******************************************************************************
 * Software License Agreement
 *
 * Copyright (c) 2012 by John J. McDonough, WB8RCR
 *
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 *
 *****************************************************************************/

//	Function Prototypes
int main ( void );
void Initialize( void );
void Banner( void );
void DemoLCDposition( void );
void DemoLCDleft( void );
void DemoLCDright( void );
void DemoLCDshift( void );
void DemoLongScroll( void );
void EndingArrow( void );

//! main - Exercise the LCD while varying the brightness of the LEDs
/*! Exercise the LCD library
 *  Demonstrate all the key features of the LCD library.  Show
 *  straight text display and clear, cursor position, move left
 *  move right, scroll.
 */
int main ( void )
{

  Initialize();
  Banner();

  while (1)
    {
      DemoLCDposition();
      DemoLCDleft();
      DemoLCDright();
      DemoLCDshift();
      DemoLongScroll();
      EndingArrow();
    }
}
