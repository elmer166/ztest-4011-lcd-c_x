/*! \file 4011-Tmr3Interrupt.c
 *
 * \brief Timer 3 interrupt service routine
 *
 * This routine adjusts the brightness of the LEDs by
 * varying the PWM duty cycle.  Really kind of unnecessary
 * in this application.
 * 
 */

#include <p30Fxxxx.h>
#include <math.h>

//! Angle used to vary the intensity of the LCDs
extern float theta;

//! Full circle of LED brightness
#define TWOPI 6.28318536
//! Offset between brightness of each LED
#define PIOVER4 TWOPI/8.0

//! Timer 3 Interrupt Service Routine
/*! Gets executed whenever Timer 3 expires.
 * 
 * Adjusts the brightness of the LEDs based on the global variable
 * theta.  Theta is incremented each interrupt and limited to
 * the range of zero to two pi.  LEDs are on when the port is false
 * and off when the port is true.  Since the LEDs are quite bright,
 * the duty cycle is kept in the upper range, different for each
 * LED, to make the brightness appear similar but not too bright.
 */
void __attribute__((__interrupt__, auto_psv)) _T3Interrupt( void)
{
  _T3IF = 0; // Clear timer 3 interrupt flag

  // Set the brightness of each LED based on the sine
  // of the angle.  The order here looks a little odd
  // because the position of the LEDs on the board differs
  // from the order of the port numbers
  OC3RS = (int) (928.0 -  96.0 * sin (theta - PIOVER4));    // green
  OC4RS = (int) (960.0 -  64.0 * sin (theta));              // yellow
  OC2RS = (int) (768.0 - 256.0 * sin (theta + PIOVER4));    // red

  theta = theta + 0.05;
  if (theta > TWOPI)
    theta = 0.0;

}
