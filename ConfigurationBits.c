/*! \file ConfigurationBits.c
 *
 *  \brief Contains the configuration fuse settings
 */
#include <p30Fxxxx.h>

// Configuration fuses
//! Use crystal oscillator, 16X PLL for 29.5 MIPS clock
_FOSC (XT_PLL16);
//! Watchdog timer off
_FWDT (WDT_OFF);
//! 16ms Power on timer, 2.7V brownout detect, enable MCLR
_FBORPOR (PWRT_16 & BORV27 & MCLR_EN);
//! No code protection
_FGS( GWRP_OFF & CODE_PROT_OFF );

