/*! \file LCDbusy.c
 *
 *  \brief Wait while LCD busy
 *
 */

#include "lcd_intern.h"
#include "delay.h"

//! Wait while LCD is busy
/*!  Monitor LCD busy flag until it becomes non-busy.  Note that
 *   we need to allow time for data to stabilize before testing.
 * \param none
 * \returns none
 *
 * \todo Update for actual busy check.  This routine is for now a
 * placeholder.  Rather than actually checking the busy bit it simply
 * delays about 200 microceconds.  When done, remove delay at end
 * of LCDcommand() and also the LCDclear() macro.
 *
 */
void LCDbusy( void )
{
    // Placeholder for code to check the busy bit
    Delay_Us(Delay200uS_count);
    return;
}
