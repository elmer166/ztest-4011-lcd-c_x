/*! \file LCDcommand.c
 *
 *  \brief Send a command to the LCD
 *
 */


#include "lcd.h"
#include "lcd_intern.h"
#include "delay.h"

//! Send a command to the LCD
/*!  This routine simple sends a data byte to the LCD.  The
 *   register select pin is set to 0 notifying the LCD that the
 *   byte is to be interpreted as a command.
 *
 * \param cmd char - Command byte to send to LCD
 * \returns none
 * 
 */
void LCDcommand( unsigned char cmd )
{
    LCDbusy();
    LCD_RS = 0;             // assert register select to 1
    LCDsend( cmd );
    LCD_RS = 1;             // negate register select to 0
    Delay(Delay_1mS_Cnt);   // 1 ms delay

}

