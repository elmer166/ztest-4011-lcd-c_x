/*! \file LCDputs.c
 *
 *  \brief Put a string to the LCD
 *
 */

#include "lcd.h"
#include "lcd_intern.h"
#include "delay.h"


//! Send a string to the LCD
/*! Sends a null-terminated string to the LCD
 * \param p char * - pointer to string to be displayed
 * \returns none
 */
void LCDputs( char *p )
{
    while (*p)
    {
        LCDletter(*p);
        p++;
    }
}
