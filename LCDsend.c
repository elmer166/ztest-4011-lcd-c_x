/*! \file LCDsend.c
 *
 *  \brief Send a byte to the LCD
 *
 */



#include "lcd.h"
#include "lcd_intern.h"
#include "delay.h"

//! LCDsend - Send a byte to the LCD

/*! Sends 8 bits of data to the LCD, pulses the LCD enable
 * bit to strobe the data in.  refactored out of LCDcommand and
 * LCDletter in preparation for switching to a 4 bit interface.
 *
 * \par data - byte to be sent
 * \return void
 */
void LCDsend( char data )
{
  LCD_DATA &= 0xFF0F;
  LCD_DATA |= ( data  ) & 0xF0;
  LCDpulseEnableBit();

  LCD_DATA &= 0xFF0F;
  LCD_DATA |=  ( data << 4)  & 0xF0;
  LCDpulseEnableBit();
}
