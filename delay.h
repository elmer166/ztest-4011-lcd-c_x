/*! \file delay.h
 *
 *   \brief Declarations for LCD delay routines
 *
 *   This header replaces the delay.h *AND* the delay.c used in
 *   earlier LCD routines.  Instead, it relies on the library
 *   routine __delay32.
 *
 *   The argument for __delay32 is the number of cycles desired,
 *   with a minimum of 12.  The routine takes into account the
 *   function call time, but not the argument setup time.  The
 *   manual suggests that the argument setup time is on the order
 *   of 2 cycles, thus the calculated counts are reduced by two
 *   in the declarafions below.
 *
 *   In the case of the dsPIC-EL, where a 7.3728 MHz crystal is used,
 *   the instruction clock can be selected from between 1.8 and
 *   29.5 MIPS.  For an LCD (only), displaying 16 times slower is
 *   barely noticeable, so using a library compiled for 29.5 MIPS
 *   is acceptible in an application compiled for 1.8 MIPS.
 */
#include <libpic30.h>

//! Instruction clock Hz
#define Fcy  30000000

//! Delay a specified time
#define Delay __delay32
//! Delay a shorter time (same function as Delay() used)
#define Delay_Us __delay32

//! Counts for a 200 us delay
#define Delay200uS_count  ((unsigned long)(Fcy * 0.0002)-2)
//! Counts for a 1 ms delay
#define Delay_1mS_Cnt	  ((unsigned long)(Fcy * 0.001)-2)
//! Counts for a 2 ms delay
#define Delay_2mS_Cnt	  ((unsigned long)(Fcy * 0.002)-2)
//! Counts for a 5 ms delay
#define Delay_5mS_Cnt	  ((unsigned long)(Fcy * 0.005)-2)
//! Counts for a 15 ms delay
#define Delay_15mS_Cnt 	  ((unsigned long)(Fcy * 0.015)-2)
//! Counts for a 1 second delay
#define Delay_1S_Cnt	  ((unsigned long)(Fcy * 1)-2)

