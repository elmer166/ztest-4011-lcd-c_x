/*! \file lcd_intern.h
 *
 *  \brief Definitions used within LCD routines
 *
 * This file contains definitions of the various connections
 * to the LCD on the Explorer 16 board.  They are uninteresting
 * outside the LCD routines.
 */
/* 
 * File:   lcd_intern.h
 * Author: jjmcd
 *
 * Created on June 19, 2012, 12:57 PM
 */

#if defined(__PIC24E__)
#include <p24Exxxx.h>
#elif defined (__PIC24F__)
#include <p24Fxxxx.h>
#elif defined(__PIC24H__)
#include <p24Hxxxx.h>
#elif defined(__dsPIC30F__)
#include <p30Fxxxx.h>
#elif defined (__dsPIC33E__)
#include <p33Exxxx.h>
#elif defined(__dsPIC33F__)
#include <p33Fxxxx.h>
#endif

#ifndef LCD_INTERN_H
#define	LCD_INTERN_H

#ifdef	__cplusplus
extern "C" {
#endif

/*
   For Explorer 16 board, here are the data and control signal definitions
   RS -> RB15
   E  -> RD4
   LCD_RW -> RD5
   DATA -> RE0 - RE7
*/

// Control signal data pins
//! LCD Read/Write pin
#define  LCD_RW         LATFbits.LATF5
//! LCD Register select pin
#define  LCD_RS         LATFbits.LATF6
//! LCD Enable pin
#define  LCD_ENABLE     LATFbits.LATF4

// Control signal pin direction
//! LCD Read/Write direction register bit
#define  LCD_RW_TRIS        TRISFbits.TRISF5
//! LCD Register select direction register bit
#define  LCD_RS_TRIS        TRISFbits.TRISF6
//! LCD Enable direction register bit
#define  LCD_ENABLE_TRIS    TRISFbits.TRISF4

// Data signals and pin direction
//! LCD data port latch
#define  LCD_DATA      LATB
//! LCD data port
#define  LCD_DATAPORT  PORTB
//! LCD data port direction register
#define  LCD_DATATRIS  TRISB

//! Toggle the LCD enable bit
void LCDpulseEnableBit( void );
//! Wait for LCD not busy
void LCDbusy( void );
//! Send a byte to the LCD
void LCDsend( char );

#ifdef	__cplusplus
}
#endif

#endif	/* LCD_INTERN_H */

