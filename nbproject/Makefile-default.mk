#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/4011-LCD-C.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/4011-LCD-C.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/862518904/ConfigurationBits.o ${OBJECTDIR}/_ext/862518904/LCDbusy.o ${OBJECTDIR}/_ext/862518904/LCDcommand.o ${OBJECTDIR}/_ext/862518904/LCDcountedstring.o ${OBJECTDIR}/_ext/862518904/LCDinit.o ${OBJECTDIR}/_ext/862518904/LCDletter.o ${OBJECTDIR}/_ext/862518904/LCDpulseEnableBit.o ${OBJECTDIR}/_ext/862518904/LCDputs.o ${OBJECTDIR}/_ext/862518904/LCDsend.o ${OBJECTDIR}/_ext/862518904/4011-LCD-C.o ${OBJECTDIR}/_ext/862518904/4011-Banner.o ${OBJECTDIR}/_ext/862518904/4011-DemoLCDposition.o ${OBJECTDIR}/_ext/862518904/4011-DemoLCDleft.o ${OBJECTDIR}/_ext/862518904/4011-DemoLCDright.o ${OBJECTDIR}/_ext/862518904/4011-DemoLCDshift.o ${OBJECTDIR}/_ext/862518904/4011-DemoLongScroll.o ${OBJECTDIR}/_ext/862518904/4011-EndingArrow.o ${OBJECTDIR}/_ext/862518904/4011-Tmr3Interrupt.o ${OBJECTDIR}/_ext/862518904/4011-Initialize.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/862518904/ConfigurationBits.o.d ${OBJECTDIR}/_ext/862518904/LCDbusy.o.d ${OBJECTDIR}/_ext/862518904/LCDcommand.o.d ${OBJECTDIR}/_ext/862518904/LCDcountedstring.o.d ${OBJECTDIR}/_ext/862518904/LCDinit.o.d ${OBJECTDIR}/_ext/862518904/LCDletter.o.d ${OBJECTDIR}/_ext/862518904/LCDpulseEnableBit.o.d ${OBJECTDIR}/_ext/862518904/LCDputs.o.d ${OBJECTDIR}/_ext/862518904/LCDsend.o.d ${OBJECTDIR}/_ext/862518904/4011-LCD-C.o.d ${OBJECTDIR}/_ext/862518904/4011-Banner.o.d ${OBJECTDIR}/_ext/862518904/4011-DemoLCDposition.o.d ${OBJECTDIR}/_ext/862518904/4011-DemoLCDleft.o.d ${OBJECTDIR}/_ext/862518904/4011-DemoLCDright.o.d ${OBJECTDIR}/_ext/862518904/4011-DemoLCDshift.o.d ${OBJECTDIR}/_ext/862518904/4011-DemoLongScroll.o.d ${OBJECTDIR}/_ext/862518904/4011-EndingArrow.o.d ${OBJECTDIR}/_ext/862518904/4011-Tmr3Interrupt.o.d ${OBJECTDIR}/_ext/862518904/4011-Initialize.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/862518904/ConfigurationBits.o ${OBJECTDIR}/_ext/862518904/LCDbusy.o ${OBJECTDIR}/_ext/862518904/LCDcommand.o ${OBJECTDIR}/_ext/862518904/LCDcountedstring.o ${OBJECTDIR}/_ext/862518904/LCDinit.o ${OBJECTDIR}/_ext/862518904/LCDletter.o ${OBJECTDIR}/_ext/862518904/LCDpulseEnableBit.o ${OBJECTDIR}/_ext/862518904/LCDputs.o ${OBJECTDIR}/_ext/862518904/LCDsend.o ${OBJECTDIR}/_ext/862518904/4011-LCD-C.o ${OBJECTDIR}/_ext/862518904/4011-Banner.o ${OBJECTDIR}/_ext/862518904/4011-DemoLCDposition.o ${OBJECTDIR}/_ext/862518904/4011-DemoLCDleft.o ${OBJECTDIR}/_ext/862518904/4011-DemoLCDright.o ${OBJECTDIR}/_ext/862518904/4011-DemoLCDshift.o ${OBJECTDIR}/_ext/862518904/4011-DemoLongScroll.o ${OBJECTDIR}/_ext/862518904/4011-EndingArrow.o ${OBJECTDIR}/_ext/862518904/4011-Tmr3Interrupt.o ${OBJECTDIR}/_ext/862518904/4011-Initialize.o


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/4011-LCD-C.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=30F4011
MP_LINKER_FILE_OPTION=,-Tp30F4011.gld
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/862518904/ConfigurationBits.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/ConfigurationBits.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/ConfigurationBits.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/ConfigurationBits.o.ok ${OBJECTDIR}/_ext/862518904/ConfigurationBits.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/ConfigurationBits.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/ConfigurationBits.o.d" -o ${OBJECTDIR}/_ext/862518904/ConfigurationBits.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/ConfigurationBits.c    
	
${OBJECTDIR}/_ext/862518904/LCDbusy.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDbusy.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDbusy.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDbusy.o.ok ${OBJECTDIR}/_ext/862518904/LCDbusy.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/LCDbusy.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/LCDbusy.o.d" -o ${OBJECTDIR}/_ext/862518904/LCDbusy.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDbusy.c    
	
${OBJECTDIR}/_ext/862518904/LCDcommand.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDcommand.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDcommand.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDcommand.o.ok ${OBJECTDIR}/_ext/862518904/LCDcommand.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/LCDcommand.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/LCDcommand.o.d" -o ${OBJECTDIR}/_ext/862518904/LCDcommand.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDcommand.c    
	
${OBJECTDIR}/_ext/862518904/LCDcountedstring.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDcountedstring.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDcountedstring.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDcountedstring.o.ok ${OBJECTDIR}/_ext/862518904/LCDcountedstring.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/LCDcountedstring.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/LCDcountedstring.o.d" -o ${OBJECTDIR}/_ext/862518904/LCDcountedstring.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDcountedstring.c    
	
${OBJECTDIR}/_ext/862518904/LCDinit.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDinit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDinit.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDinit.o.ok ${OBJECTDIR}/_ext/862518904/LCDinit.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/LCDinit.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/LCDinit.o.d" -o ${OBJECTDIR}/_ext/862518904/LCDinit.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDinit.c    
	
${OBJECTDIR}/_ext/862518904/LCDletter.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDletter.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDletter.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDletter.o.ok ${OBJECTDIR}/_ext/862518904/LCDletter.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/LCDletter.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/LCDletter.o.d" -o ${OBJECTDIR}/_ext/862518904/LCDletter.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDletter.c    
	
${OBJECTDIR}/_ext/862518904/LCDpulseEnableBit.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDpulseEnableBit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDpulseEnableBit.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDpulseEnableBit.o.ok ${OBJECTDIR}/_ext/862518904/LCDpulseEnableBit.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/LCDpulseEnableBit.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/LCDpulseEnableBit.o.d" -o ${OBJECTDIR}/_ext/862518904/LCDpulseEnableBit.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDpulseEnableBit.c    
	
${OBJECTDIR}/_ext/862518904/LCDputs.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDputs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDputs.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDputs.o.ok ${OBJECTDIR}/_ext/862518904/LCDputs.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/LCDputs.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/LCDputs.o.d" -o ${OBJECTDIR}/_ext/862518904/LCDputs.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDputs.c    
	
${OBJECTDIR}/_ext/862518904/LCDsend.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDsend.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDsend.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDsend.o.ok ${OBJECTDIR}/_ext/862518904/LCDsend.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/LCDsend.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/LCDsend.o.d" -o ${OBJECTDIR}/_ext/862518904/LCDsend.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDsend.c    
	
${OBJECTDIR}/_ext/862518904/4011-LCD-C.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-LCD-C.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-LCD-C.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-LCD-C.o.ok ${OBJECTDIR}/_ext/862518904/4011-LCD-C.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/4011-LCD-C.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/4011-LCD-C.o.d" -o ${OBJECTDIR}/_ext/862518904/4011-LCD-C.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-LCD-C.c    
	
${OBJECTDIR}/_ext/862518904/4011-Banner.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-Banner.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-Banner.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-Banner.o.ok ${OBJECTDIR}/_ext/862518904/4011-Banner.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/4011-Banner.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/4011-Banner.o.d" -o ${OBJECTDIR}/_ext/862518904/4011-Banner.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-Banner.c    
	
${OBJECTDIR}/_ext/862518904/4011-DemoLCDposition.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-DemoLCDposition.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-DemoLCDposition.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-DemoLCDposition.o.ok ${OBJECTDIR}/_ext/862518904/4011-DemoLCDposition.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/4011-DemoLCDposition.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/4011-DemoLCDposition.o.d" -o ${OBJECTDIR}/_ext/862518904/4011-DemoLCDposition.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-DemoLCDposition.c    
	
${OBJECTDIR}/_ext/862518904/4011-DemoLCDleft.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-DemoLCDleft.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-DemoLCDleft.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-DemoLCDleft.o.ok ${OBJECTDIR}/_ext/862518904/4011-DemoLCDleft.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/4011-DemoLCDleft.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/4011-DemoLCDleft.o.d" -o ${OBJECTDIR}/_ext/862518904/4011-DemoLCDleft.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-DemoLCDleft.c    
	
${OBJECTDIR}/_ext/862518904/4011-DemoLCDright.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-DemoLCDright.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-DemoLCDright.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-DemoLCDright.o.ok ${OBJECTDIR}/_ext/862518904/4011-DemoLCDright.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/4011-DemoLCDright.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/4011-DemoLCDright.o.d" -o ${OBJECTDIR}/_ext/862518904/4011-DemoLCDright.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-DemoLCDright.c    
	
${OBJECTDIR}/_ext/862518904/4011-DemoLCDshift.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-DemoLCDshift.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-DemoLCDshift.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-DemoLCDshift.o.ok ${OBJECTDIR}/_ext/862518904/4011-DemoLCDshift.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/4011-DemoLCDshift.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/4011-DemoLCDshift.o.d" -o ${OBJECTDIR}/_ext/862518904/4011-DemoLCDshift.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-DemoLCDshift.c    
	
${OBJECTDIR}/_ext/862518904/4011-DemoLongScroll.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-DemoLongScroll.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-DemoLongScroll.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-DemoLongScroll.o.ok ${OBJECTDIR}/_ext/862518904/4011-DemoLongScroll.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/4011-DemoLongScroll.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/4011-DemoLongScroll.o.d" -o ${OBJECTDIR}/_ext/862518904/4011-DemoLongScroll.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-DemoLongScroll.c    
	
${OBJECTDIR}/_ext/862518904/4011-EndingArrow.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-EndingArrow.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-EndingArrow.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-EndingArrow.o.ok ${OBJECTDIR}/_ext/862518904/4011-EndingArrow.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/4011-EndingArrow.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/4011-EndingArrow.o.d" -o ${OBJECTDIR}/_ext/862518904/4011-EndingArrow.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-EndingArrow.c    
	
${OBJECTDIR}/_ext/862518904/4011-Tmr3Interrupt.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-Tmr3Interrupt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-Tmr3Interrupt.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-Tmr3Interrupt.o.ok ${OBJECTDIR}/_ext/862518904/4011-Tmr3Interrupt.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/4011-Tmr3Interrupt.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/4011-Tmr3Interrupt.o.d" -o ${OBJECTDIR}/_ext/862518904/4011-Tmr3Interrupt.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-Tmr3Interrupt.c    
	
${OBJECTDIR}/_ext/862518904/4011-Initialize.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-Initialize.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-Initialize.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-Initialize.o.ok ${OBJECTDIR}/_ext/862518904/4011-Initialize.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/4011-Initialize.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/4011-Initialize.o.d" -o ${OBJECTDIR}/_ext/862518904/4011-Initialize.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-Initialize.c    
	
else
${OBJECTDIR}/_ext/862518904/ConfigurationBits.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/ConfigurationBits.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/ConfigurationBits.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/ConfigurationBits.o.ok ${OBJECTDIR}/_ext/862518904/ConfigurationBits.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/ConfigurationBits.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/ConfigurationBits.o.d" -o ${OBJECTDIR}/_ext/862518904/ConfigurationBits.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/ConfigurationBits.c    
	
${OBJECTDIR}/_ext/862518904/LCDbusy.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDbusy.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDbusy.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDbusy.o.ok ${OBJECTDIR}/_ext/862518904/LCDbusy.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/LCDbusy.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/LCDbusy.o.d" -o ${OBJECTDIR}/_ext/862518904/LCDbusy.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDbusy.c    
	
${OBJECTDIR}/_ext/862518904/LCDcommand.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDcommand.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDcommand.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDcommand.o.ok ${OBJECTDIR}/_ext/862518904/LCDcommand.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/LCDcommand.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/LCDcommand.o.d" -o ${OBJECTDIR}/_ext/862518904/LCDcommand.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDcommand.c    
	
${OBJECTDIR}/_ext/862518904/LCDcountedstring.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDcountedstring.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDcountedstring.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDcountedstring.o.ok ${OBJECTDIR}/_ext/862518904/LCDcountedstring.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/LCDcountedstring.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/LCDcountedstring.o.d" -o ${OBJECTDIR}/_ext/862518904/LCDcountedstring.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDcountedstring.c    
	
${OBJECTDIR}/_ext/862518904/LCDinit.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDinit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDinit.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDinit.o.ok ${OBJECTDIR}/_ext/862518904/LCDinit.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/LCDinit.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/LCDinit.o.d" -o ${OBJECTDIR}/_ext/862518904/LCDinit.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDinit.c    
	
${OBJECTDIR}/_ext/862518904/LCDletter.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDletter.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDletter.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDletter.o.ok ${OBJECTDIR}/_ext/862518904/LCDletter.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/LCDletter.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/LCDletter.o.d" -o ${OBJECTDIR}/_ext/862518904/LCDletter.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDletter.c    
	
${OBJECTDIR}/_ext/862518904/LCDpulseEnableBit.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDpulseEnableBit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDpulseEnableBit.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDpulseEnableBit.o.ok ${OBJECTDIR}/_ext/862518904/LCDpulseEnableBit.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/LCDpulseEnableBit.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/LCDpulseEnableBit.o.d" -o ${OBJECTDIR}/_ext/862518904/LCDpulseEnableBit.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDpulseEnableBit.c    
	
${OBJECTDIR}/_ext/862518904/LCDputs.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDputs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDputs.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDputs.o.ok ${OBJECTDIR}/_ext/862518904/LCDputs.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/LCDputs.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/LCDputs.o.d" -o ${OBJECTDIR}/_ext/862518904/LCDputs.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDputs.c    
	
${OBJECTDIR}/_ext/862518904/LCDsend.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDsend.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDsend.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/LCDsend.o.ok ${OBJECTDIR}/_ext/862518904/LCDsend.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/LCDsend.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/LCDsend.o.d" -o ${OBJECTDIR}/_ext/862518904/LCDsend.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/LCDsend.c    
	
${OBJECTDIR}/_ext/862518904/4011-LCD-C.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-LCD-C.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-LCD-C.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-LCD-C.o.ok ${OBJECTDIR}/_ext/862518904/4011-LCD-C.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/4011-LCD-C.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/4011-LCD-C.o.d" -o ${OBJECTDIR}/_ext/862518904/4011-LCD-C.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-LCD-C.c    
	
${OBJECTDIR}/_ext/862518904/4011-Banner.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-Banner.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-Banner.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-Banner.o.ok ${OBJECTDIR}/_ext/862518904/4011-Banner.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/4011-Banner.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/4011-Banner.o.d" -o ${OBJECTDIR}/_ext/862518904/4011-Banner.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-Banner.c    
	
${OBJECTDIR}/_ext/862518904/4011-DemoLCDposition.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-DemoLCDposition.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-DemoLCDposition.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-DemoLCDposition.o.ok ${OBJECTDIR}/_ext/862518904/4011-DemoLCDposition.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/4011-DemoLCDposition.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/4011-DemoLCDposition.o.d" -o ${OBJECTDIR}/_ext/862518904/4011-DemoLCDposition.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-DemoLCDposition.c    
	
${OBJECTDIR}/_ext/862518904/4011-DemoLCDleft.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-DemoLCDleft.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-DemoLCDleft.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-DemoLCDleft.o.ok ${OBJECTDIR}/_ext/862518904/4011-DemoLCDleft.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/4011-DemoLCDleft.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/4011-DemoLCDleft.o.d" -o ${OBJECTDIR}/_ext/862518904/4011-DemoLCDleft.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-DemoLCDleft.c    
	
${OBJECTDIR}/_ext/862518904/4011-DemoLCDright.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-DemoLCDright.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-DemoLCDright.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-DemoLCDright.o.ok ${OBJECTDIR}/_ext/862518904/4011-DemoLCDright.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/4011-DemoLCDright.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/4011-DemoLCDright.o.d" -o ${OBJECTDIR}/_ext/862518904/4011-DemoLCDright.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-DemoLCDright.c    
	
${OBJECTDIR}/_ext/862518904/4011-DemoLCDshift.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-DemoLCDshift.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-DemoLCDshift.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-DemoLCDshift.o.ok ${OBJECTDIR}/_ext/862518904/4011-DemoLCDshift.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/4011-DemoLCDshift.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/4011-DemoLCDshift.o.d" -o ${OBJECTDIR}/_ext/862518904/4011-DemoLCDshift.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-DemoLCDshift.c    
	
${OBJECTDIR}/_ext/862518904/4011-DemoLongScroll.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-DemoLongScroll.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-DemoLongScroll.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-DemoLongScroll.o.ok ${OBJECTDIR}/_ext/862518904/4011-DemoLongScroll.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/4011-DemoLongScroll.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/4011-DemoLongScroll.o.d" -o ${OBJECTDIR}/_ext/862518904/4011-DemoLongScroll.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-DemoLongScroll.c    
	
${OBJECTDIR}/_ext/862518904/4011-EndingArrow.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-EndingArrow.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-EndingArrow.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-EndingArrow.o.ok ${OBJECTDIR}/_ext/862518904/4011-EndingArrow.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/4011-EndingArrow.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/4011-EndingArrow.o.d" -o ${OBJECTDIR}/_ext/862518904/4011-EndingArrow.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-EndingArrow.c    
	
${OBJECTDIR}/_ext/862518904/4011-Tmr3Interrupt.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-Tmr3Interrupt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-Tmr3Interrupt.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-Tmr3Interrupt.o.ok ${OBJECTDIR}/_ext/862518904/4011-Tmr3Interrupt.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/4011-Tmr3Interrupt.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/4011-Tmr3Interrupt.o.d" -o ${OBJECTDIR}/_ext/862518904/4011-Tmr3Interrupt.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-Tmr3Interrupt.c    
	
${OBJECTDIR}/_ext/862518904/4011-Initialize.o: /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-Initialize.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/862518904 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-Initialize.o.d 
	@${RM} ${OBJECTDIR}/_ext/862518904/4011-Initialize.o.ok ${OBJECTDIR}/_ext/862518904/4011-Initialize.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/862518904/4011-Initialize.o.d" $(SILENT) -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -MMD -MF "${OBJECTDIR}/_ext/862518904/4011-Initialize.o.d" -o ${OBJECTDIR}/_ext/862518904/4011-Initialize.o /home/jjmcd/Projects/PIC/dsPIC/4011-LCD-C.X/4011-Initialize.c    
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/4011-LCD-C.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -omf=elf -mcpu=$(MP_PROCESSOR_OPTION)  -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -o dist/${CND_CONF}/${IMAGE_TYPE}/4011-LCD-C.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}         -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__ICD2RAM=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_ICD3=1
else
dist/${CND_CONF}/${IMAGE_TYPE}/4011-LCD-C.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -omf=elf -mcpu=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/4011-LCD-C.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}         -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION)
	${MP_CC_DIR}/pic30-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/4011-LCD-C.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} -omf=elf
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
